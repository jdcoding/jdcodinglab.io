module Main exposing (..)

import Browser
import Html exposing (Html, button, div, input, text)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)


main: Program () Model Msg
main =
  Browser.sandbox { init = init, update = update, view = view }

type alias Model = 
  { text: String
  , number: Int 
  , name: String
  , password: String
  , passwordAgain: String
  }

init : Model
init = 
  { text = "Text to reverse"
  , number = 0
  , name = ""
  , password = ""
  , passwordAgain = ""
  }

view : Model -> Html Msg
view model =
  div []
    [ text "Welcome to the homepage of Johannes Döllinger"
    , div [] 
      [ text "Elm Tutorial"
      , div []
        [ exercise1View model.number
        , exercise2View model.text
        , exercise3View model.name model.password model.passwordAgain
        ]
      ]
    ]


exercise1View : Int -> Html Msg
exercise1View displayNumber = 
  div []
  [ text "Exercise 1: Buttons:"
  , div [] 
    [ button [ onClick (MsgExercise1Msg Decrement1) ] [ text "-1" ]
    , button [ onClick (MsgExercise1Msg Decrement10) ] [text "-10" ]
    ]
  , div [] 
    [ text (String.fromInt displayNumber) ]
  , div [] 
    [ button [ onClick (MsgExercise1Msg Increment1) ] [ text "+1" ]
    , button [ onClick (MsgExercise1Msg Increment10) ] [text "+10" ]
    ]
  , div [] 
    [ button [ onClick (MsgExercise1Msg Reset) ] [ text "Reset" ] ]
  ]

type Msg
  = MsgExercise1Msg Exercise1Msg
  | MsgExercise2Msg Exercise2Msg String
  | MsgExercise3Msg Exercise3Msg String

type Exercise1Msg
  = Increment1
  | Increment10
  | Decrement1
  | Decrement10
  | Reset

exercise2View : String -> Html Msg
exercise2View textToReverse =
  div []
  [ text "Exercise 2: Text Fields:"
  , div []
    [ input [ placeholder "Text to reverse", value textToReverse, onInput (MsgExercise2Msg Change) ] []
  , div [] 
    [ text ("Reversed text: " ++ String.reverse textToReverse) ]
    , text ("Length: " ++ String.fromInt (String.length textToReverse))
    ]
  ]

type Exercise2Msg
  = Change

exercise3View : String -> String -> String -> Html Msg
exercise3View name password passwordAgain =
  div []
  [ text "Exercise 3: Forms:"
  , div []
    [ viewInput "text" "Name" name (MsgExercise3Msg Name)
    , viewInput "password" "Password" password (MsgExercise3Msg Password)
    , viewInput "password" "Re-enter Password" passwordAgain (MsgExercise3Msg PasswordAgain)
    , div []
        (viewValidation password passwordAgain)
    ]
  ]

type Exercise3Msg
  = Name
  | Password
  | PasswordAgain

viewInput : String -> String -> String -> (String -> msg) -> Html msg
viewInput t p v toMsg =
  input [ type_ t, placeholder p, value v, onInput toMsg ] []

viewValidation : String -> String -> List (Html msg)
viewValidation password passwordAgain =
  if List.length (errorMessages password passwordAgain) /= 0 then
    errorMessages password passwordAgain
  else
    [div [ style "color" "green" ] [ text "OK" ]]


errorMessages : String -> String -> List (Html msg)
errorMessages password passwordAgain = 
  List.map redText (
    List.map .errorMessage (
      List.filter (\validationCriterion -> validationCriterion.checkFunction password passwordAgain)
      [ { checkFunction = \p1 -> \p2 -> String.length p1 < 8
        , errorMessage = "Password must be at least 8 characters long!"
        }
      , { checkFunction = \p1 -> \p2 -> noCharFulfills Char.isLower p1
        , errorMessage = "Password must have at least one lowercase character!"
        }
      , { checkFunction = \p1 -> \p2 -> noCharFulfills Char.isUpper p1
        , errorMessage = "Password must have at least one uppercase character!"
        }
      , { checkFunction = \p1 -> \p2 -> noCharFulfills Char.isDigit p1
        , errorMessage = "Password must have at least one numeric character!"
        }
      , { checkFunction = \p1 -> \p2 -> p1 /= p2 
        , errorMessage = "Passwords do not match!"
        }
      ]
    )
  )

noCharFulfills : (Char -> Bool) -> String -> Bool
noCharFulfills booleanFunction string =
  String.length (String.filter booleanFunction string) == 0

redText : String -> Html msg
redText string = 
  div [ style "color" "red" ] [ text string ]

update : Msg -> Model -> Model
update msg model =
  case msg of
    MsgExercise1Msg msg1 ->
      updateExercise1 msg1 model

    MsgExercise2Msg msg2 string ->
      updateExercise2 msg2 model string

    MsgExercise3Msg msg3 string ->
      updateExercise3 msg3 model string

updateExercise1 : Exercise1Msg -> Model -> Model
updateExercise1 msg model = 
  case msg of
    Increment1 ->
      { model | number = model.number + 1 }

    Increment10 ->
      { model | number = model.number + 10 }

    Decrement1 ->
      { model | number = model.number - 1 }

    Decrement10 ->
      { model | number = model.number - 10 }

    Reset ->
      { model | number = 0 }
    
updateExercise2 : Exercise2Msg -> Model -> String -> Model
updateExercise2  msg model newContent = 
  { model | text = newContent }

updateExercise3 : Exercise3Msg -> Model -> String -> Model
updateExercise3 msg model string = 
  case msg of 
    Name -> 
      { model | name = string }
    
    Password ->
      { model | password = string }

    PasswordAgain ->
      { model | passwordAgain = string }
